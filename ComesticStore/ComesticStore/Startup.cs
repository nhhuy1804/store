﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ComesticStore.Startup))]
namespace ComesticStore
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
